CC=	gcc
CFLAGS=	-O2

common:
	mkdir -p bin

naturradius: common
	${CC} -o bin/naturradius ${CFLAGS} naturradius.c

clean:
	rm -rf bin
