#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/* Autor: glowiak
 * Program do obliczania liczb naturalnych w danym zakresie.
 * Na potrzeby zadania 5/11.
 */

int main(int argc, char* argv[]);
bool divideable(int low, int upp);
bool natural(int val);

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Użycie: ./naturradius <podłoga> <sufit>\n");
		return -1;
	}

	int low = (int)strtol(argv[1], NULL, 10);
	int upp = (int)strtol(argv[2], NULL, 10);

	if (low >= upp)
	{
		printf("Pierwsza liczba musi być mniejsza od drugiej!\n");
		return -1;
	}
	if (low == 0 || upp == 0 || low == 1 || upp == 1)
	{
		printf("Żadna z liczb nie może być równa 0 lub 1!\n");
		return -1;
	}
	
	for (int i = low; i <= upp; i++)
		if (natural(i))
			printf("%d\n", i);

	return 0;
}
bool divideable(int low, int upp)
{
	for (int i = 0; i < upp; i++)
		if (low * i == upp)
			return true;
	if (low == upp)
		return true;

	return false;
}
bool natural(int val)
{
	for (int i = val; i > 1; i--)
		if (i != val && divideable(i, val))
			return false;

	return true;
}
